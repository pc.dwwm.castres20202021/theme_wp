
<?php
if( ! function_exists( 'script' ) ):
    function script() {
        wp_enqueue_style( 'style', get_stylesheet_uri());
    }
    endif;

    add_action( 'wp_enqueue_scripts', 'script');


function foo_modify_query_order( $query ) {
	
 	if ( $query->is_home() && $query->is_main_query() ) {

 		$query->set( 'orderby', 'title' );

 		$query->set( 'order', 'ASC' );
 	}

 }
 add_action( 'pre_get_posts', 'foo_modify_query_order' );

 function general_admin_notice(){
    global $pagenow;
    if ( $pagenow == 'index.php' ) {

         echo '<div class="notice notice-warning is-dismissible">
             <p>You must add ACF plugin to your wordpress.</p>
         </div>';
    }
}



add_action('admin_notices','general_admin_notice' );
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_60269c51b879c',
        'title' => 'betes',
        'fields' => array(
            array(
                'key' => 'field_60269d17a9b4e',
                'label' => 'taille',
                'name' => 'taille',
                'type' => 'number',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => '',
                'max' => '',
                'step' => '',
            ),
            array(
                'key' => 'field_60269d3ea9b4f',
                'label' => 'enluminure',
                'name' => 'img',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => 'field_60269d60a9b50',
                'label' => 'mange',
                'name' => 'mange',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => '',
                'taxonomy' => '',
                'filters' => array(
                    0 => 'search',
                    1 => 'post_type',
                    2 => 'taxonomy',
                ),
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;
 ?>
 