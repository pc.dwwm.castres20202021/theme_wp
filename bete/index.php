
    <?php get_header(); ?>

  
<body>
    <div>
        <h1>Index :</h1>

        <div>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <ul>
                <li>
                    <a href="<?php the_permalink(); ?>" class="lien">
                        <?php the_title('<h2>', '</h2>'); ?>
                    </a>
                </li>
            </ul>
            <?php endwhile;
            else : _e('désolé aucun post ne corespond.', 'textdomain');
            endif; ?>    
        </div>
    </div>
    
    <?php get_footer(); ?>
