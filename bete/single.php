<?php get_header(); ?>

<div class="principale">

    <h1 class="titre"><?php the_title(); ?></h1>
    <?php 
$image = get_field('enluminure');
if( !empty( $image ) ): ?>
    <img src="<?php echo esc_url($image['enluminure']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
<?php endif; ?>
    <!-- <img class="img" src="<?php echo get_field('enluminure'); ?>"/> -->

    <?php the_content(); ?>
    <h3>Bestiole:</h3>
    <ul>

        <a href="<?php echo get_field('betes'); ?>">
            <li><?php echo get_field('enluminure'); ?></li>
        </a>
    </ul>
    </p>
    <p>
    <h3>Repas:</h3>
    <?php $mange = get_field('mange'); ?>
    <!-- <?php var_dump ($mange); ?> -->
    <ul>
        <?php foreach ($mange as $betes) { ?>
        
            <a href="<?php echo $betes->guid; ?>">
                <li><?php echo $betes->post_title; ?></li>
            </a>
        <?php } ?>


    </ul>
    </p>

</div>
<?php get_footer(); ?>